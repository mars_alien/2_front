import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClButton } from 'mp-colorui'
import './login.scss'

@inject('app')
@observer
class Login extends Taro.Component {

  constructor(props) {
    super(props)
  }

  config = {
    navigationBarTitleText: '登录'
  }

  componentDidShow() {
    // reset
    this.setState({ loginStatus: false })
  }

  onLogin = () => {
    const { identify } = this.state
    const { app } = this.props
    this.setState({
      loginStatus: true
    }, async () => {
      try {
        await app.login({ identify, })
        Taro.redirectTo({ url: '/pages/index/index' }) // 登陆成功, 跳转
      } catch (e) {
        Taro.showToast({ title: '登陆失败，请联系管理员', icon: 'none' })
      }
    })
  }

  render() {
    const { loginStatus } = this.state
    return (
      <View className='app-page login-root'>
        <View className='app-btn-group login-btn-root'>
          <ClButton long size='large' bgColor='cyan' onClick={this.onLogin} loading={loginStatus}>微信登陆</ClButton>
        </View>
      </View>
    )
  }
}

export default Login
