import Taro, { Component } from '@tarojs/taro'
import moment from 'dayjs'
import { View, Image, Map } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClAccordion } from 'mp-colorui'

import './activity.scss'
import {getImagePath, calcDuration, formatDate } from "../../utils";

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      info: [],
      movement: [],
      locations: [],
      open: false
    }
  }

  componentDidMount () {
    this.getDate()
  }

  getDate = async () => {
    const { params: { id } } = this.$router;
    const { app: { getActivity } } = this.props
    const currentActivity = await getActivity({ activity_id: id })
    Taro.setNavigationBarTitle({ title: '活动总览' })
    this.setState({
      info: [{
        label: '名称',
        content: currentActivity.title
      }, {
        label: '开始时间',
        content: formatDate(currentActivity.start_time),
      }, {
        label: '结束时间',
        content: formatDate(currentActivity.end_time),
      }],
      movement: this.getMoment(currentActivity),
      locations: this.getLocations(currentActivity),
      open: true
    })
  }

  getMoment = (data) => {
    return data.photo_info.map(item => {
      return {
        message: item.description,
        time: formatDate(item.create_time),
        url: getImagePath(item.image_id)
      }
    })
  }

  getLocations = data => {
    return data.position_info.map(item => {
      const coordinate = item.start_coordinate.split(',')
      return {
        longitude: coordinate[0],
        latitude: coordinate[1],
        time: formatDate(item.start_time),
        duration: calcDuration(item.start_time, item.end_time),
        markers: [{
          longitude: coordinate[0],
          latitude: coordinate[1],
          label: {
            content: item.title
          }
        }]
      }
    })
  }

  onSelect = () => {
    Taro.navigateTo({ url: '/pages/activity/detail' })
  }

  onAddClick = () => {
    Taro.navigateTo({ url: '/pages/activity/add' })
  }

  render() {
    const { info, movement, locations, open } = this.state;
    return (
      <View className='activity-overview'>
        <ClAccordion title='信息' card='true' open={open} className='activity-overview__card'>
          <View>
            {
              info.map((item, index) => (
                <View className='app-list-item horizon' key={`base-info-${index}`}>
                  <View className='app-list-item__label'>{item.label}</View>
                  <View className='app-list-item__content'>{item.content}</View>
                </View>
              ))
            }
          </View>
        </ClAccordion>
        <ClAccordion title='动态' card='true' open={open} className='activity-overview__card'>
          <View>
            {
              movement.map((item, index) => (
                <View className='activity-overview__image' key={`images-${index}`}>
                  <Image src={item.url} className='image' mode='aspectFill' />
                  <View className='content'>{item.message}</View>
                  <View className='time'>{item.time}</View>
                </View>
              ))
            }
          </View>
        </ClAccordion>
        <ClAccordion title='轨迹' card='true' open={open} className='activity-overview__card'>
          <View>
            {
              locations.map((item, index) => (
                <View className='activity-overview__location' key={`location-${index}`}>
                  <Map className='activity-overview__map' enable-zoom={false} enable-scroll={false} longitude={item.longitude} latitude={item.latitude} markers={item.markers} />
                  <View className='time'>{item.time || ''}</View>
                  <View className='duration'>{item.duration || '0'} min</View>
                </View>
              ))
            }
          </View>
        </ClAccordion>
      </View>
    )
  }
}

export default Index
