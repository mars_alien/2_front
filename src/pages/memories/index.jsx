import Taro, { Component } from '@tarojs/taro'
import { View, Map, Canvas } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClButton } from 'mp-colorui'
import './index.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: {},
      trace: []
    }
  }

  componentDidMount () {
    Taro.setNavigationBarTitle({ title: '我的回忆' })
    this.getDate()
  }

  getDate = async () => {
    const { app } = this.props
    const data = await app.getStatistics()
    data['totalTime'] = (Number(data['total_time']) / 60).toFixed(2)
    this.setState({
      data,
      trace: data.trace.map(item => {
        const coordinate = item.coordinate.split(',')
        return {
          coordinate: item.coordinate,
          longitude: coordinate[0],
          latitude: coordinate[1],
          label: { content: '活动: ' + item.activity_title }
        }
      })
    })
  }

  onComplete = async () => {
    const { app } = this.props
    const { data, trace } = this.state
    const ctx = Taro.createCanvasContext('canvas', this.$scope)
    ctx.setFontSize(18)
    ctx.fillStyle = '#fff'
    ctx.fillRect(0, 0, 680, 720)
    ctx.fillStyle = '#2b2b2b'
    ctx.fillText(`参与次数: ${data.counts}`, 8, 30)
    ctx.fillText(`总共时长: ${data.totalTime} min`, 8, 70)
    ctx.fillText(`最长一次服务: ${data.longest_activity.activity.title}`, 8, 110)
    ctx.fillText(`感悟最多: ${data.most_photos.activity.title}`, 8, 150)
    ctx.fillText('活动轨迹', 8, 190)
    if (trace.length) {
      try {
        const traceMap = await app.fetchMapImage(trace[0].coordinate, trace)
        console.log(traceMap)
        ctx.drawImage(traceMap.tempFilePath, 0, 220, 400, 400)
      } catch(e) {}
    }
    ctx.draw(false, () => {
      Taro.canvasToTempFilePath({
        x: 0,
        y: 0,
        width: 680,
        height: 720,
        canvasId: 'canvas',
        success: (res) => {
          if (res.errMsg === 'canvasToTempFilePath:ok') {
            Taro.saveImageToPhotosAlbum({
              filePath: res.tempFilePath
            })
          }
        }
      }, this.$scope)
    })
  }

  render() {
    const { data, trace } = this.state
    if (data.counts === 0) {
      return null
    }
    return (
      <View className='memories-list'>
        <View>
          <View className='app-list-item horizon'>
            <View className='app-list-item__label'>参与次数</View>
            <View className='app-list-item__content'>{data.counts || ''}</View>
          </View>
          <View className='app-list-item horizon'>
            <View className='app-list-item__label'>总共时长</View>
            <View className='app-list-item__content'>{data.totalTime || ''} min</View>
          </View>
          <View className='app-list-item horizon'>
            <View className='app-list-item__label'>最长一次服务</View>
            <View className='app-list-item__content'>{data.longest_activity.activity.title}</View>
          </View>
          <View className='app-list-item horizon'>
            <View className='app-list-item__label'>感悟最多</View>
            <View className='app-list-item__content'>{data.most_photos.activity.title}</View>
          </View>
          {
            trace && (
              <View className='app-list-item'>
                <View className='app-list-item__label'>活动轨迹</View>
                <Map className='memories-list__map' include-points={trace} markers={trace} longitude={trace[0].longitude} latitude={trace[0].latitude} enable-zoom enable-scroll={false}  />
              </View>
            )
          }
        </View>
        <Canvas className='memories-list__canvas' style='display: none, width: 680px; height: 720px;' canvasId='canvas'>
        </Canvas>
        {/*<ClButton className='memories-list__btn' long size='large' bgColor='cyan' onClick={this.onComplete}>导出图片</ClButton>*/}
      </View>
    )
  }
}

export default Index
