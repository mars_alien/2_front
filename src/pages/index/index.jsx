import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClTabBar } from "mp-colorui";

import History from '../history';
import Memories from '../memories';
import Activity from '../activity';
import './index.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.nav = [{
      badge: false,
      title: '成长',
      icon: 'activity',
    }, {
      badge: false,
      title: '收藏',
      icon: 'like',
    }, {
      badge: false,
      title: '回忆',
      icon: 'tag',
    }];
    this.state = {
      active: 0,
    }
  }

  config = {
    navigationBarTitleText: '社区服务共享'
  }

  onChangeActive = (id) => {
    this.setState({ active: id })
  }

  render() {
    const { active } = this.state;
    return (
      <View className='index-root'>
        <View className='index-content'>
          { active === 0 ? <Activity /> : null }
          { active === 1 ? <History /> : null }
          { active === 2 ? <Memories /> : null }
        </View>
        <ClTabBar
          activeColor='cyan'
          active={active}
          tabs={this.nav}
          onClick={this.onChangeActive}
        />
      </View>
    )
  }
}

export default Index 
