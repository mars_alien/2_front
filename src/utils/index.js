import Taro from '@tarojs/taro'
import moment from 'dayjs'
import appStore from '../store/app'

const HOST = 'https://shangde.xianglinhu.com'

export async function request(url, method, data, noToken = false){
  const { buyerToken } = appStore
  let fullUrl = `${HOST}${url}`
  if (!noToken && buyerToken ) {
    if (buyerToken.search('admin') !== -1) {
      fullUrl += `?token=${buyerToken}&shop_id=2`
    } else {
      fullUrl += `?buyer_token=${buyerToken}&shop_id=2`
    }
  }
  Taro.showLoading({ title: '加载中', mask: true })
  const result = await Taro.request({
    url: fullUrl, data, method,
    header: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  Taro.hideLoading()

  if (result && result.data && result.data.code === 0) {
    // for login
    if (result.data.buyer && result.data.buyer.buyer_id) {
      return {
        token: result.data.data,
        buyer: result.data.buyer
      }
    }
    return result.data.data || {}
  }

  if (result.data.code === 1007) {
    Taro.navigateTo({ url: '/pages/login/login' })
    throw new Error('no_login')
  } else {
    throw new Error(result.data.msg)
  }
}

export function getImagePath(id) {
  return `${HOST}/statics/images/${id}`
}

export function formatDate (date, template = 'YYYY-MM-DD HH:mm') {
  if (!date) {
    return ''
  }
  return moment.unix(date).format(template)
}

export function calcDuration (start, end) {
  if (start && end) {
    start = moment.unix(start)
    end = moment.unix(end)
    return end.diff(start, 'minute')
  }
  return ''
}
