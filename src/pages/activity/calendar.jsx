import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import moment from 'dayjs'
import { observer, inject } from '@tarojs/mobx'
import { ClCalendar, ClMenuList, ClModal, ClInput, ClForm, ClSelect } from 'mp-colorui';

import './activity.scss'
import {formatDate} from '../../utils';

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activityDay: '',
      menu: [
        { title: '设置备注', },
        { title: '设置活动日期', },
      ],
      tipDay: [],
      noticeShow: false,
      noticeDate: '',
      noticeContent: '',
      changeShow: false,
      newActivityDay: '',
      newActivityTime: '',
      loading: 0,
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '活动日历' })
  }

  getData = async() => {
    const { app: { getActivity } } = this.props
    const { params: { id }} = this.$router

    this.setState({ loading: true })

    let result = await getActivity({ activity_id: id })
    let addition = {}
    const activityDay = formatDate(result.start_time, 'YYYY-MM-DD')
    try { addition = JSON.parse(result.date_addition) } catch(e) { }
    this.setState({
      id: result.activity_id,
      activityDay,
      addition,
      tipDay: this.getTipDay(addition, activityDay),
      loading: false,
    })
  }

  componentDidShow() {
    const current = moment()
    this.setState({ noticeDate: current.format('YYYY-MM-DD') })
    this.getData()
  }

  getTipDay = (addition, activityDay) => {
    addition = Object.assign({}, addition)
    // set activity day
    addition[activityDay] = {
      ...addition[activityDay],
      date: activityDay,
      tipTop: '开始时间',
      tipTopColor: 'red',
    }
    return Object.keys(addition).map(item => addition[item])
  }

  onSelectDate = (value) => {
    console.log(value);
    return this.state.activityDay;
  }

  onNoticeContentChange = value => {
    this.setState({ noticeContent: value })
  }

  onNoticeDateChange = value => {
    this.setState({ noticeDate: value })
  }

  onNewActivityDayChange = value => {
    this.setState({ newActivityDay: value })
  }

  onNewActivityTimeChange = value => {
    this.setState({ newActivityTime: value })
  }

  showNoticeModal = value => {
    this.setState({ noticeShow: value })
  }

  showChangeModal = value => {
    let data = { changeShow: value }
    if (value === true) {
      data = {
        ...data,
        newActivityDay: moment().format('YYYY-MM-DD'),
        newActivityTime: moment().format('HH:mm')
      }
    }
    this.setState(data)
  }

  onMenuClick = index => {
    if (index === 0) {
      this.showNoticeModal(true)
    }
    if (index === 1) {
      this.showChangeModal(true)
    }
  }

  onNoticeConfirm = async () => {
    const { app } = this.props
    const { noticeDate, noticeContent, id, addition } = this.state
    const newAddition = {
      ...addition,
      [noticeDate]: {
        date: noticeDate,
        tipTop: '备注',
        tipBottom: noticeContent,
      }
    }
    try {
      await app.changeActivity({ activity_id: id, date_addition: JSON.stringify(newAddition)})
      Taro.showToast({ title: '记录成功', icon: 'success', complete: () => {
          this.showNoticeModal(false)
          this.getData()
      }})
    } catch(e) {
      Taro.showToast({ title: '操作失败', icon: 'none' })
    }
  }

  onChangeConfirm = async () => {
    const { app } = this.props
    const { newActivityDay, newActivityTime,id } = this.state
    const startTime = `${newActivityDay} ${newActivityTime}`
    try {
      await app.changeActivity({ activity_id: id, start_time: startTime})
      Taro.showToast({ title: '修改成功', icon: 'success', complete: () => {
          this.showChangeModal(false)
          this.getData()
        }})
    } catch(e) {
      Taro.showToast({ title: '操作失败', icon: 'none' })
    }
  }

  onChangeModalClick = index => {
    if (index === 0) {
      this.showChangeModal(false)
    }
    if (index === 1) {
      this.onChangeConfirm()
    }
  }

  onNoticeModalClick = index => {
    if (index === 0) {
      this.showNoticeModal(false)
    }
    if (index === 1) {
      this.onNoticeConfirm()
    }
  }

  onDayClick = (date) => {
    const { tipDay } = this.state
    const result = tipDay.filter(item => item.date === date)
    if (result.length && result[0].tipBottom) {
      Taro.showModal({ title: result[0].tipBottom, })
    }
  }

  render() {
    const {
      menu, noticeShow, noticeContent,
      noticeDate, changeShow, activityDay,
      newActivityDay, newActivityTime, tipDay, loading
    } = this.state;

    if (loading) {
      return null
    }

    return (
      <View className='activity-add'>
        <ClCalendar
          backToToday={false}
          tipDay={tipDay}
          specialDay={[activityDay]}
          onSelectDate={this.onSelectDate}
          showType='full'
          highlightWeekend
          activeColor='cyan'
          onClick={this.onDayClick}
        />
        <ClModal
          show={noticeShow}
          title='添加备注'
          actions={[
            { text: '取消', color: 'red' },
            { text: '记录', color: 'blue' }
          ]}
          onCancel={() => { this.showNoticeModal(false) }}
          onClose={() => { this.showNoticeModal(false) }}
          onClick={this.onNoticeModalClick}
        >
          <ClForm>
            <View className='app-list-item'>
              <View className='app-list-item__label'>日期</View>
              <View className='app-list-item__content'>
                <ClSelect mode='date' date={{ value: noticeDate }} onChange={this.onNoticeDateChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>备注</View>
              <View className='app-list-item__content'>
                <ClInput value={noticeContent} onChange={this.onNoticeContentChange} />
              </View>
            </View>
          </ClForm>
        </ClModal>
        <ClModal
          show={changeShow}
          title='修改活动开始日期'
          actions={[
            { text: '取消', color: 'red' },
            { text: '确定', color: 'blue' }
          ]}
          onCancel={() => { this.showChangeModal(false) }}
          onClose={() => { this.showChangeModal(false) }}
          onClick={this.onChangeModalClick}
        >
          <ClForm>
            <View className='app-list-item'>
              <View className='app-list-item__label'>开始日期</View>
              <View className='app-list-item__content'>
                <ClSelect mode='date' date={{ value: newActivityDay }} onChange={this.onNewActivityDayChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>开始时间</View>
              <View className='app-list-item__content'>
                <ClSelect mode='time' time={{ value: newActivityTime }} onChange={this.onNewActivityTimeChange} />
              </View>
            </View>
          </ClForm>
        </ClModal>
        <ClMenuList list={menu} className='app-margin-top' onClick={this.onMenuClick} />
      </View>
    )
  }
}

export default Index
