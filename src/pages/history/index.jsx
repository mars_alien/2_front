import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClTimeline } from 'mp-colorui'
import './index.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props)
    this.state = {
      history: []
    }
  }

  componentDidMount () {
    Taro.setNavigationBarTitle({ title: '活动历史' })
    this.getData()
  }

  getData = async () => {
    const { app } = this.props;
    await app.fetchActivityList({ status: 1, buyer_id: app.buyer.buyer_id })
    this.setState({
      history: app.activities.map(item => ({ ...item, bgColor: 'cyan' }))
    })
  }

  onTimelineClick = (value) => {
    const { history } = this.state;
    const result = history.filter((_, index) => index == value);
    if (result.length) {
      Taro.navigateTo({ url: `/pages/activity/overview?id=${result[0].activity_id}` })
    }
  }

  render() {
    const { history } = this.state
    return (
      <View className='history-list'>
        <ClTimeline times={history} onClick={this.onTimelineClick} className='history-list__timeline' />
      </View>
    )
  }
}

export default Index
