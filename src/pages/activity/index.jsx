import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClFloatButton } from 'mp-colorui'

import './activity.scss'
import {getImagePath} from "../../utils";

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount () {
    Taro.setNavigationBarTitle({ title: '社区服务共享' })
    this.getData()
  }

  getData = () => {
    const { app } = this.props
    const { fetchActivityList, buyer } = app
    fetchActivityList({ status: 0, buyer_id: buyer.buyer_id })
  }

  onSelect = (id) => {
    Taro.navigateTo({ url: `/pages/activity/detail?id=${id}` })
  }

  onAddClick = () => {
    Taro.navigateTo({ url: '/pages/activity/add' })
  }

  render() {
    const { app: { activities } } = this.props;
    return (
      <View className='activity-list'>
        {
          activities.map(item => (
            <View key={item.id} className='activity-list__card' onClick={() => this.onSelect(item.activity_id)}>
              <Image className='activity-card__img' src={getImagePath(item.image_id)} mode='aspectFill' />
              <Text className='activity-card__text'>{item.title}</Text>
            </View>
          ))
        }
        <ClFloatButton
          size='large'
          bgColor='red'
          direction='vertical'
          open={false}
          onClick={this.onAddClick}
        />
      </View>
    )
  }
}

export default Index
