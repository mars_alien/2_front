import Taro, { Component } from '@tarojs/taro'
import { observer, inject } from '@tarojs/mobx'
import moment from 'dayjs'
import { View } from '@tarojs/components'
import { ClSwiper, ClMenuList, ClButton } from 'mp-colorui'

import './activity.scss'
import {getImagePath} from "../../utils";

@inject('app')
@observer
class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: 0,
      images: [],
      menu: [
        {
          target: 'calendar',
          title: '活动日历',
          arrow: true,
        },
        {
          target: 'images',
          title: '照片动态',
          arrow: true
        },
        {
          target: 'location',
          title: 'GPS 记录',
          arrow: true
        }
      ]
    }
  }

  componentDidMount() {
    const { params: { id }} = this.$router
    Taro.setNavigationBarTitle({ title: '活动详情' })
    this.setState({ id: id, }, () => this.getData() )
  }

  async getData() {
    const { params: { id }} = this.$router
    const { app: { getActivity } } = this.props
    const result = await getActivity({ activity_id: id })
    this.setState({
      images: [{ url: getImagePath(result.image_id)}]
    })
  }

  onMenuClick = value => {
    const { menu, id } = this.state
    const result = menu.filter((_, index) => value == index)
    Taro.navigateTo({ url: `/pages/activity/${result[0].target}?id=${id}` })
  }

  onComplete = async () => {
    const { app } = this.props
    const { id } = this.state
    try {
      await app.changeActivity({ activity_id: id, status: 1, end_time: moment().format('YYYY-MM-DD HH:mm') })
      Taro.showToast({ title: '活动已完成', icon: 'success' })
      setTimeout(() => { Taro.redirectTo({ url: '/pages/index/index' }) }, 1000)
    } catch(e) {
      Taro.showToast({ title: '操作失败', icon: 'none' })
    }
  }

  render() {
    const { images, menu } = this.state
    return (
      <View className='activity-detail'>
        <ClSwiper
          className='activity-detail-cover'
          list={images}
          circular
          indicatorDots
          autoplay
        />
        <ClMenuList list={menu} className='app-margin-top' onClick={this.onMenuClick} />
        <ClButton long bgColor='cyan' className='app-bottom-btn' onClick={this.onComplete}>完成活动</ClButton>
      </View>
    )
  }
}

export default Index
