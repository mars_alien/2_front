import Taro from '@tarojs/taro';
import { observable, action } from 'mobx'
import { request } from "../utils";

/* status
1. 已付款
2. 以取货
3. 以归还
*/
const HOST = 'https://shangde.xianglinhu.com'

const Store = observable({

  // 消息
  @observable
  message: {},

  @observable
  buyer: {},

  @action.bound
  showMessage (config) {
    this.message = {
      type: config.type || 'success',
      message: config.message,
      show: true
    };
    // clear message
    setTimeout(() => { this.message = {} }, 1000)
  },

  // 登陆
  loginStatus: false, // login status
  buyerToken: null, // user auth token
  userInfo: {}, // user info

  // 登陆
  async login() {
    // 获取 js_code
    const result = await Taro.login();
    if (result.errMsg !== 'login:ok') throw new Error('query js token failed')
    // 登陆
    const data = await request(
      '/api/v1/plugins/wechat/mini_app/login/query',
      'get',
      { shop_id: 2, js_code: result.code },
      true
    )
    this.buyerToken = data.token
    this.buyer = data.buyer
    Taro.setStorageSync('buyer_token', this.buyerToken)
  },

  @observable
  activities: [],

  @observable
  currentActivity: {},

  @action.bound
  async createActivity(data) {
    return await request('/api/v1/community/activity/create', 'post', data)
  },

  @action.bound
  async fetchActivityList(data) {
    this.activities = []
    const list = await request('/api/v1/community/activity/query', 'get', data)
    this.activities = list
  },

  @action.bound
  async getActivity(data) {
    this.currentActivity = {}
    const result= await request('/api/v1/community/activity/query_one', 'get', data)
    this.currentActivity = result
    return result;
  },

  @action.bound
  async changeActivity(data) {
    return await request('/api/v1/community/activity/update', 'post', data)
  },

  @action.bound
  async addImage(data) {
    return await request('/api/v1/community/photo/create', 'post', data)
  },

  @action.bound
  async fetchImagesList(data) {
    return await request('/api/v1/community/photo/query', 'get', data)
  },

  @action.bound
  async addLocation(data) {
    return await request('/api/v1/community/position/create', 'post', data)
  },

  @action.bound
  async updateLocation(data) {
    return await request('/api/v1/community/position/update', 'post', data)
  },

  @action.bound
  async getStatistics() {
    return await request('/api/v1/community/statistics/query', 'get')
  },

  @action.bound
  async uploadFile(files) {
    const { buyerToken } = this;
    Taro.showLoading({ title: '上传中', mask: true })
    const result = await Taro.uploadFile({
      url: `${HOST}/api/v1/plugins/image/create?buyer_token=${buyerToken}&shop_id=2`,
      header: {
        'content-type': 'multipart/form-data'
      },
      filePath: files[0].url,
      name: 'image',
    })
    Taro.hideLoading()
    const data = JSON.parse(result.data || '')
    if (data.code === 0) {
      return data.data
    } else {
      throw new Error('upload file failed')
    }
  },

  @action.bound
  async fetchMapImage(center, trace) {
    // 交换经纬度
    const swap = coordinate => {
      const arr = coordinate.split(',')
      return [arr[1], arr[0]].join(',')
    }
    const traceStr = trace.map(item => item.coordinate ? swap(item.coordinate) : '').join('|')
    let result = await Taro.downloadFile({
      url: `https://apis.map.qq.com/ws/staticmap/v2/?center=${swap(center)}&zoom=16&size=400*400&maptype=roadmap&markers=size:large|color:0xFFCCFF|label:k|${traceStr}&key=LJ7BZ-YKNWF-COTJY-NY7NT-U2KKH-NKF45`
    })
    return result
  },
})

export default Store
