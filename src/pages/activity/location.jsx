import Taro, { Component } from '@tarojs/taro'
import { View, Map } from '@tarojs/components'
import moment from 'dayjs'
import { observer, inject } from '@tarojs/mobx'
import { ClButton, ClModal, ClForm, ClInput } from "mp-colorui";

import './activity.scss'
import {calcDuration, formatDate} from "../../utils";

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      current: {},
      id: '',
      show: false,
      title: '',
      description: '',
      active: -1,
    }
  }

  componentDidMount () {
    const { params: { id }} = this.$router
    this.setState({ id }, async () => {
      await this.getData()
    })
    Taro.setNavigationBarTitle({ title: '活动轨迹' })
  }

  componentDidShow() {
    this.getData()
  }

  getData = async () => {
    const location = await Taro.getLocation({ type: 'gcj02' });
    const { app: { getActivity } } = this.props
    const { params: { id }} = this.$router

    let result = await getActivity({ activity_id: id })
    // 获取所有轨迹记录
    const locations = result.position_info.map(item => {
      const _result = {
        location : item.title,
        time: formatDate(item.create_time),
      }
      if (item.end_time) {
        _result.duration = calcDuration(item.create_time, item.end_time)
        _result.endTime = formatDate(item.end_time)
      }
      return _result
    })
    // 判断是否有未结束活动
    const activeActivity = result.position_info.filter(item => !item.end_time)
    // 组装地图 marker
    const markers = result.position_info.map((item, index) => {
      const coordinate = item.start_coordinate.split(',')
      return {
        id: index,
        latitude: parseFloat(coordinate[1]),
        longitude: parseFloat(coordinate[0]),
        label: {
          content: item.title
        }
      }
    })
    if (location) {
      markers.push(location)
    }

    this.setState({
      current: { longitude: location.longitude, latitude: location.latitude },
      locations,
      markers,
      active: activeActivity.length ? activeActivity[0].position_id : -1
    })
  }

  showModal = value => {
    this.setState({ show: value })
  }

  onTitleChange = (title) => {
    this.setState({ title })
  }

  onDescriptionChange = description => {
    this.setState({ description })
  }

  onConfirm = async () => {
    const { app } = this.props
    const { description, title, id, current } = this.state
    const time = moment()
    try {
      await app.addLocation({
        activity_id: id,
        title: title,
        description: description,
        start_time: time.format('YYYY-MM-DD HH:mm:ss'),
        start_coordinate: `${current.longitude},${current.latitude}`,
      })
      Taro.showToast({ title: '操作成功', icon: 'success' })
    } catch(e) {
      Taro.showToast({ title: '操作失败', icon: 'none' })
    }
    this.showModal(false)
    await this.getData()
  }

  onModalClick = index => {
    if (index === 0) {
      this.showModal(false)
    }
    if (index === 1) {
      this.onConfirm()
    }
  }

  onComplete = async () => {
    const { app } = this.props
    const { active } = this.state
    const location = await Taro.getLocation({ type: 'gcj02' });
    const time = moment()
    try {
      await app.updateLocation({
        position_id: active,
        end_time: time.format('YYYY-MM-DD HH:mm:ss'),
        end_coordinate: `${location.longitude},${location.latitude}`,
      })
      Taro.showToast({ title: '更新记录成功', icon: 'success', complete: () => {
        this.getData()
      }})
    } catch(e) {
      Taro.showToast({ title: '更新记录失败', icon: 'none' })
    }
    await this.getData()
  }

  render() {
    const { locations, current, markers, show, title, description, active } = this.state;
    return (
      <View className='activity-location'>
        <Map className='activity-location__map'
          longitude={current.longitude}
          latitude={current.latitude}
          markers={markers}
        />
        <View className='activity-location__list'>
        {
          locations.map((item, index) => (
            <View key={`locations-${index}`} className='activity-location__item'>
              <View className='location'>地点: {item.location}</View>
              <View className='time'>{item.time}</View>
              {
                item.endTime && <View className='duration'>持续时长: {item.duration || '0'} min </View>
              }
            </View>
          ))
        }
        </View>
        <ClModal
          show={show}
          title='记录轨迹'
          actions={[
            { text: '取消', color: 'red' },
            { text: '记录', color: 'blue' }
          ]}
          onCancel={() => { this.showModal(false) }}
          onClose={() => { this.showModal(false) }}
          onClick={this.onModalClick}
        >
          <ClForm>
            <View className='app-list-item'>
              <View className='app-list-item__label'>地点</View>
              <View className='app-list-item__content'>
                <ClInput value={title} onChange={this.onTitleChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>描述</View>
              <View className='app-list-item__content'>
                <ClInput value={description} onChange={this.onDescriptionChange} />
              </View>
            </View>
          </ClForm>
        </ClModal>
        {
          active === -1 && <ClButton className='app-bottom-btn' onClick={() => this.showModal(true)}>开始记录</ClButton>
        }
        {
          active !== -1 && <ClButton bgColor='red' className='app-bottom-btn' onClick={this.onComplete}>标记结束</ClButton>
        }
      </View>
    )
  }
}

export default Index
