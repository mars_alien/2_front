import Taro, { Component } from '@tarojs/taro'
import momment from 'dayjs'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClForm, ClSelect, ClCard, ClImagePicker, ClButton, ClTextarea } from "mp-colorui";

import './activity.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      images: [],
      description: '',
      startTime: '',
      startDate: '',
    }
  }

  config = {
    navigationBarTitleText: '发布动态'
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '发布动态' })
    const { params: { id }} = this.$router
    const current = momment()
    this.setState({
      id,
      startDate: current.format('YYYY-MM-DD'),
      startTime: current.format('HH:mm')
    })
  }

  onUpload = async (data) => {
    const { app } = this.props;
    const result = await app.uploadFile(data)
    this.setState(({ images: [ { url: result, status: 'success' }] }))
  }

  onDescriptionChange = (description) => {
    this.setState({ description })
  }

  onStartTimeChange = (startTime) => {
    this.setState({ startTime })
  }

  onStartDateChange = (startDate) => {
    this.setState({ startDate })
  }

  onConfirm = async () => {
    const { app } = this.props
    const { id, images, description, startTime, startDate } = this.state;
    try {
      await app.addImage({
        activity_id: id,
        title: 'add images',
        description: description,
        log_time: `${startDate} ${startTime}`,
        image_id: images[0].url
      })
      Taro.showToast({ title: '创建图片记录成功', icon: 'success',
        complete: () => { Taro.navigateBack() }
      })
    } catch(e) {
      Taro.showToast({ title: '创建图片记录失败', icon: 'none' })
    }
  }

  render() {
    const { images, description, startTime, startDate } = this.state;
    return (
      <View className='activity-add'>
        <ClCard>
          <ClForm>
            <View className='app-list-item'>
              <View className='app-list-item__label'>动态时间</View>
              <View className='app-list-item__content'>
                <ClSelect mode='date' title='日期' date={{value: startDate}} onChange={this.onStartDateChange} />
                <ClSelect mode='time' title='时间' time={{value: startTime}} onChange={this.onStartTimeChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>感想</View>
              <View className='app-list-item__content'>
                <ClTextarea bgColor='light-gray' placeholder='请输入感想与反思' autoFocus value={description} onChange={this.onDescriptionChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>长传图片</View>
              <View className='app-list-item__content no-bg no-padding'>
                <ClImagePicker imgList={images} chooseImgObj={{ count: 1, success: this.onUpload}} />
              </View>
            </View>
          </ClForm>
        </ClCard>
        <ClButton long bgColor='cyan' className='app-bottom-btn' onClick={this.onConfirm}>发布</ClButton>
      </View>
    )
  }
}

export default Index
