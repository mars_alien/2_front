import Taro, { Component } from '@tarojs/taro'
import moment from 'dayjs'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { ClSelect, ClForm, ClInput, ClCard, ClImagePicker, ClButton, ClTextarea } from 'mp-colorui';
import './activity.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      startTime: '',
      startDate: '',
      images: {},
    }
  }

  config = {
    navigationBarTitleText: '创建服务'
  }

  componentDidShow() {
    const current= moment()
    this.setState({
      startTime: current.format('HH:mm'),
      startDate: current.format('YYYY-MM-DD')
    })
  }

  onUpload = async (data) => {
    const { app } = this.props;
    const result = await app.uploadFile(data)
    this.setState({ images: [ { url: result, status: 'success' }] })
  }

  onStartTimeChange = (startTime) => {
    this.setState({ startTime })
  }

  onStartDateChange = (startDate) => {
    this.setState({ startDate })
  }

  onTitleChange = (title) => {
    this.setState({ title })
  }

  onDescriptionChange = (description) => {
    this.setState({ description })
  }

  onCreate = async () => {
    const { app } = this.props
    const { title, description, startDate, startTime, images } = this.state;
    try {
      await app.createActivity({
        title,
        description,
        image_id: images[0].url,
        start_time: `${startDate} ${startTime}`,
        date_addition: '',
      })
      Taro.showToast({ title: '创建活动成功', icon: 'success', complete: () => {
        Taro.redirectTo({ url: '/pages/activity/index' })
      }})
    } catch (e) {
      console.log(e)
      Taro.showToast({ title: '创建活动失败', icon: 'none' })
    }
  }

  render() {
    const { title, description, startDate, startTime, images } = this.state
    return (
      <View className='activity-add'>
        <ClCard>
          <ClForm>
            <View className='app-list-item'>
              <View className='app-list-item__label'>服务名称</View>
              <View className='app-list-item__content'>
                <ClInput placeholder='请输入服务名称' autoFocus value={title} onChange={this.onTitleChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>参与人员</View>
              <View className='app-list-item__content'>
                <ClTextarea bgColor='light-gray' placeholder='请输入参与人员' value={description} onChange={this.onDescriptionChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>开始时间</View>
              <View className='app-list-item__content'>
                <ClSelect mode='date' title='选择日期' date={{ value: startDate }} onChange={this.onStartDateChange} />
                <ClSelect mode='time' title='选择时间' time={{ value: startTime }} onChange={this.onStartTimeChange} />
              </View>
            </View>
            <View className='app-list-item'>
              <View className='app-list-item__label'>插入背景</View>
              <View className='app-list-item__content no-bg no-padding'>
                <ClImagePicker imgList={images} max={1} chooseImgObj={{ count: 1, success: this.onUpload}} />
              </View>
            </View>
          </ClForm>
        </ClCard>
        <ClButton long bgColor='cyan' className='app-bottom-btn' onClick={this.onCreate}>创建</ClButton>
      </View>
    )
  }
}

export default Index
