import Taro, { Component } from '@tarojs/taro'
import { observer, inject } from '@tarojs/mobx'
import { View, Image, Text } from '@tarojs/components'
import { ClFloatButton } from 'mp-colorui'

import './activity.scss'
import {getImagePath} from "../../utils";

@inject('app')
@observer
class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      images: [],
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '照片动态'})
    const { params: { id }} = this.$router
    this.setState({ id }, async () => {
      await this.getData()
    })
  }

  componentDidShow() {
    this.getData()
  }

  getData = async() => {
    const { app: { getActivity } } = this.props
    const { params: { id }} = this.$router
    let result = await getActivity({ activity_id: id })
    this.setState({
      images: result.photo_info.map(item => {
        return { url: getImagePath(item.image_id), message: item.description}
      })
    })
  }

  onAddClick = () => {
    const { id } = this.state
    Taro.navigateTo({ url: `/pages/activity/images_add?id=${id}` })
  }

  render() {
    const { images } = this.state
    return (
      <View className='activity-images'>
        {
          images.map((item, index) => (
            <View key={`image-${index}`} className='activity-images__item'>
              <Image src={item.url} mode='aspectFill' />
              <Text>{item.message}</Text>
            </View>
          ))
        }
        <ClFloatButton
          size='large'
          bgColor='red'
          direction='vertical'
          open={false}
          onClick={this.onAddClick}
        />
      </View>
    )
  }
}

export default Index
